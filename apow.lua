--[[
Annotation^
v1.0
--]]

local setmetatable = setmetatable

local mt = {__pow = function(self, other)
    return self.annotate(self, other)
  end}
local annotate = function(self, target)
  return setmetatable({annotate = target}, mt)
end
local annotation = annotate(nil, annotate)

return {annotation = annotation}